// console.log("Hello World");

// ARRAY METHODS

	// JavaScript has built-in functions and methods for arrays. This allows us to mainpulate and access array elements.

// [SECTION] Mutator Methods
	// Mutator methods are funcions that mutate or change an array after they've been created.
	// These methods manipulate the original array performing various tasks as adding and removing elements.

	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit']
	console.log(fruits);

	// First Mutator Method: push()
		// Adds an element/s in the end of an array and returns the array's length
		/*
			Syntax:
				arrayName.push(elementToBeAdded);
		*/

	console.log("Current Array Fruits []:");
	console.log(fruits);

	let fruitsLength = fruits.push("Mango");
	console.log("Mutated Aray after the push method:")
	console.log(fruits);
	console.log(fruitsLength);

	// Adding multiple elements to an array
	fruits.push("Avocado", "Guava");
	console.log("Mutated Aray after the push method:")
	console.log(fruits);


	// Second Mutator Method: pop()
		// Removes the last element in an array and returns the removed element
		/*
			Syntax:
				arrayName.pop();
		*/

	console.log("Current Array Fruits []:");
	console.log(fruits);

	let removedFruit = fruits.pop()
	console.log("Mutated Aray after the push method:")
	console.log(fruits);
	console.log(removedFruit);


	// Third Mutator Method: unshift()
		// Adds one or more elements at the beginning of an array and returns the present length.
		/*
			Syntax:
				arrayName.unshift(elementToBeAdded);
		*/

	console.log("Current Array Fruits []:");
	console.log(fruits);


	fruitsLength = fruits.unshift('Lime', 'Banana');
	console.log("Mutated Aray after the push method:")
	console.log(fruits);
	console.log(fruitsLength);



	// Fourth Mutator Method: shift()
		// It removes an element at the beginning of an array and returns the removed element.
		/*
			Syntax:
				arrayName.shift();
		*/

	console.log("Current Array Fruits []:");
	console.log(fruits);

	removedFruit = fruits.shift();
	console.log("Mutated Aray after the push method:")
	console.log(fruits);
	console.log(removedFruit);



	// Fifth Mutator Method: splice()
		// it simultaneously removes elements from a specified index number and adds elements.
		/*
			Syntax:
				arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
		*/

	console.log("Current Array Fruits []:");
	console.log(fruits);

	let splice = fruits.splice(1, 2, 'Lime', 'Guava');
	console.log("Mutated Aray after the push method:")
	console.log(fruits);
	console.log(splice);

	fruits.splice(1,2);
	console.log(fruits);

	fruits.splice(2,0, 'Durian', 'Santol');
	console.log(fruits);



	// Sixth Mutator Method: sort()
		// rearranges the array element in alphanumeric order
		/*
			Syntax:
				arrayName.sort();
		*/

	console.log("Current Array Fruits []:");
	console.log(fruits);

	fruits.sort();
	console.log("Mutated Aray after the push method:")
	console.log(fruits);
	console.log(fruits[0]);



	// Seventh Mutator Method: reverse()
		// reverses the order of array elements
		/*
			Syntax:
				arrayName.reverse();
		*/

	console.log("Current Array Fruits []:");
	console.log(fruits);

	fruits.reverse();
	console.log("Mutated Aray after the push method:")
	console.log(fruits);


// [SECTION] Non-mutator Methods
	// Non-mutator are functions that do not modify or change an array after they are created

	// These methods do not manipulate the original array performing task such as returning elements from an array and combining arrays and printing the output.

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH',
		'FR', 'DE'];


	// First Non-Mutator Method: indexOf()
		// it returns the index number of the first matching element found in an array
		// if no match was found, the result will be -1
		// the search process will be done from the first element proceeding to the last element
		/*
			Syntax:
				arrayName.indexOf(searchValue);
		
		*/

	console.log(countries);
	console.log(countries.indexOf('PH'));
	console.log(countries.indexOf('BR'));

		// in indexOf() we can set the starting index

	console.log(countries.indexOf('PH',2));



	// Second Non-Mutator Method: lastIndexOf()
		// it returns the index number of the last matching element found in an array
		// if no match was found, the result will be -1
		// the search process will be done from the last element proceeding to the first element
		/*
			Syntax:
				arrayName.lastIndexOf(searchValue);
		
		*/

	console.log(countries.lastIndexOf('PH'));



	// Third Non-Mutator Method: slice()
		// portions/slices from array and returs a new array
		/*
			Syntax:
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex,endingIndex);
		*/

		let slicedArrayA = countries.slice(2);
		console.log(slicedArrayA);
		console.log(countries);

		let slicedArrayB = countries.slice(1, 5);
		console.log(slicedArrayB);



	// Fourth Non-Mutator Method: soString()
		// return an array as string separated by comma
		/*
			Syntax:
				arrayName.toString();
		*/

		let stringArray = countries.toString();
		console.log(stringArray);




	// Fifth Non-Mutator Method: concat()
		// combines arrays and returns the combined result
		/*
			Syntax:
				arrayA.concat(ArrayB);
				arrayA.concat(elementA);
		*/

		let taskArrayA = ['drink HTML', 'eat JavaScript'];
		let taskArrayB = ['inhale CSS', 'breathe SASS'];
		let taskArrayC = ['get git', 'be node'];

		let tasks = taskArrayB.concat(taskArrayA);
		console.log(tasks);

		// adding/combining multiple arrays
		let allTasks = taskArrayA.concat(taskArrayC, taskArrayB);
		console.log(allTasks);

		// combine arrays with elements
		let combinedTasks = taskArrayA.concat(taskArrayB, 'smell express', 'throw react');
		console.log(combinedTasks);



	// Sixth Non-Mutator Method: join()
		// returns an array as string separated by specified separator string
		/*
			Syntax:
				arrayName.join('separatorString');
		*/


	let users = ['John', 'Jane', 'Joe', 'Robert']
	console.log(users.join())
	console.log(users.join(" "))
	console.log(users.join(" - "))



// [SECTION] Iteration Methods
	// iteration methods are loop designs to perform repetitive tasks on arrays
	// iteration method loop over all elements in an array


	// First Iteration Method: forEach()
		// similar to for loop that iterates on each of array element
		// for each element in the array, the function in the forEach method will be ran.
		/*
			Syntax:
				arrayName.forEach(function(indivElement){
					statement/statements;
				})
		*/

	console.log(allTasks);

	let filteredTask = [];
	allTasks.forEach(function(task){
		if(task.length > 10){

			filteredTask.push(task);
		}
	})

	console.log(filteredTask);



	// Second Iteration Method: map()
		// iterates on each element and returns new array with different values depending on the result of the function's operation
		/*
			Syntax:
				arrayName.map(function(element){
					statement/statements;
					return;
				})
		*/

	let numbers = [1, 2, 3, 4, 5];
	console.log(numbers)

	let numberMap = numbers.map(function(number){
		return number*number
	})

	console.log(numberMap);



	// Third Iteration Method: every()
		// checks if all elements in an array meet the given condition.
		// this is useful in validating data stored in arrays especially when dealing with large amounts of data.
		// returns 'true' if ALL elements meet the condition and false if otherwise.
		/*
			Syntax:
				arrayName.every(function(element){
					return expression/condition;
				})
		*/

	console.log(numbers);
	let allValid = numbers.every(function(number){
		return (number < 5)
	})

	console.log(allValid);



	// Fourth Iteration Method: some()
		// checks if at least one element in the array meets the given condition.
		// returns 'true' if at least one element meets the condition and 'false' if none.
		/*
			Syntax:
				arrayName.some(function(element){
					return expression/condition;
				})
		*/

	console.log(numbers);
	let someValid = numbers.some(function(number){
		return (number < 0);
	})
	console.log(someValid);



	// Fifth Iteration Method: filter()
		// return a new array that contains the elements which meets the given condition
		// return empty array if no element were found
		/*
			Syntax:
				arrayName.filter(function(element){
					return expression/condition;
				})
		*/

	console.log(numbers);
	let filterValid = numbers.filter(function(number){
		return number <= 3;
	})

	console.log(filterValid)



	// Sixth Iteration Method: includes()
		// includes checks if the argument passed can be found in the array
		// ir returns boolean
			// true if the argument is found
			// false if not
		/*
			Syntax:
				arrayName.includes(argument);
		*/

	let products = ['mouse', 'keyboard', 'laptop', 'monitor'];

	let productFound1 = products.includes('mouse');
	console.log(productFound1);

	let productFound2 = products.includes('Mouse');
	console.log(productFound2);



	// Seventh Iteration Method: reduce()
		// evaluate elements from left to right and returns the reduced array.
		// array will turn into single value
		/*
			Syntax:
				arrayName.reduce(function(accumulator, currentValue){
					return operation/expression;
				});
		*/

	console.log(numbers);
	let total = numbers.reduce(function(x, y){
		console.log("This is the value of x: " + x)
		console.log("This is the value of y: " + y)
		return x*y;
	})

	console.log(total);